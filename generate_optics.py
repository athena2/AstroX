#!/bin/env python
arm_s='''
COMPONENT MM_arm_{index} = Arm()
AT(0,0,0) RELATIVE optics_arm
ROTATED (0,0,RAD2DEG*MM_azimuth[{index}-1]) RELATIVE optics_arm'''

mirror_mod_s='''
COMPONENT  MM_alpha_{MM_index} = MirrorModule(
  draw_individual_pores=1,
  w=pore_width, h=pore_height, l=MM_length[{MM_index}-1],radius=MM_r[{MM_index}-1],
  wall_sides=pore_seperation, wall_bottom_top=pore_seperation,
  reflection_left=mirror_coating2, reflection_right=mirror_coating2,
  reflection_bottom=mirror_coating2,reflection_top=mirror_coating,
  length_curvature_type="straight",Nw=MM_Nw[{MM_index}-1], Nh=MM_Nh[{MM_index}-1]
  )
AT (0,MM_r[{MM_index}-1]+MM_dy[{MM_index}-1],-MM_dz[{MM_index}-1]) RELATIVE MM_arm_{MM_index}
ROTATED (RAD2DEG*alpha[{MM_index}-1],0,0) RELATIVE MM_arm_{MM_index}
EXTEND
%{{
    if (flag_through_wall) flag_through_wall_alpha=1;
    if(SCATTERED>1) flag_alpha=1;
    scatters_alpha=SCATTERED;
%}}
'''
mirror_mod2_s='''
COMPONENT  MM_3alpha_{MM_index} = MirrorModule(
  draw_individual_pores=1,
  w=pore_width, h=pore_height, l=MM_length[{MM_index}-1],radius=MM_r[{MM_index}-1],
  wall_sides=pore_seperation, wall_bottom_top=pore_seperation,
  reflection_left=mirror_coating2, reflection_right=mirror_coating2,
  reflection_bottom=mirror_coating2,reflection_top=mirror_coating,
  length_curvature_type="straight",Nw=MM_Nw[{MM_index}-1], Nh=MM_Nh[{MM_index}-1]
  )
AT (0,MM_r[{MM_index}-1],0) RELATIVE MM_arm_{MM_index}
ROTATED (RAD2DEG*3*alpha[{MM_index}-1],0,0) RELATIVE MM_arm{MM_index}
EXTEND
%{{
    if (flag_through_wall) flag_through_wall_3alpha=1;
    if(SCATTERED>1) flag_3alpha=1;
    scatters_3alpha=SCATTERED;
%}}
'''


for idx in range(1,163):
    print arm_s.format(index=idx)

for idx in range(1,163):
    print mirror_mod_s.format(MM_index=idx)

#for idx in range(1,163):
#    print mirror_mod2_s.format(MM_index=idx)
