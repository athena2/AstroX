#!/bin/bash
#porenums=(3 8 14 20 26 32 38 45 53 61 69 78 88 98 108 120 132 144 157 171) 
porenums=(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20)
#parent_suffix="1line"
parent_prefix="athena_1line_mm"




parent=${parent_prefix}_rotx
if [ -d $parent ]; then
    echo $parent exists - reusing...
else
    mkdir $parent
fi

for ROT in 0 10 30 60 120 300 600 1200
do
    outdir="${parent}/rotx${ROT}arcsec"
    if [ -d ${outdir} ]; then
        echo $outdir exists - reusing...
    else
        mkdir ${outdir}
    fi
    if [ -a ${outdir}/barycenters.dat ]; then
        echo moving old barycenter file out of the way
        mv ${outdir}/barycenters.dat ${outdir}/barycenters.bak
    fi 
    for i in `seq 0 19`
    do
        if [ ! -d ${outdir}/${i} ]; then
                mxrun --mpi=2 -d${outdir}/${i}  ATHENA_1mm.instr dPrx=$ROT dHrx=$ROT SRC=3 porenumber=${porenums[$i]} hyper=1 para=1 positionfile=ATHENA_mm_1line.dat 
        fi
        gawk -- '/^[^#]/{sumx+=$3*$1; sumy+=$4*$1; sump+=$1} END {print sumx/sump,sumy/sump, sumx/sump/12*3600/3.1415927, sumy/sump/12*3600/3.1415927}' ${outdir}/${i}/FLmond_list.p.U1.x.y.k.E >> ${outdir}/barycenters.dat
    done
done

parent=${parent_prefix}_roty
if [ -d $parent ]; then
    echo $parent exists - reusing...
else
    mkdir $parent
fi

for ROT in 0 10 30 60 120 300 600 1200
do
    outdir="${parent}/roty${ROT}arcsec"
    if [ -d ${outdir} ]; then
        echo $outdir exists - reusing...
    else
        mkdir ${outdir}
    fi
    if [ -a ${outdir}/barycenters.dat ]; then
        echo moving old barycenter file out of the way
        mv ${outdir}/barycenters.dat ${outdir}/barycenters.bak
    fi 
    for i in `seq 0 19`
    do
        if [ ! -d ${outdir}/${i} ]; then
                mxrun --mpi=2 -d${outdir}/${i}  ATHENA_1mm.instr dPry=$ROT dHry=$ROT SRC=3 porenumber=${porenums[$i]} hyper=1 para=1 positionfile=ATHENA_mm_1line.dat
        fi
        gawk -- '/^[^#]/{sumx+=$3*$1; sumy+=$4*$1; sump+=$1} END {print sumx/sump,sumy/sump, sumx/sump/12*3600/3.1415927, sumy/sump/12*3600/3.1415927}' ${outdir}/${i}/FLmond_list.p.U1.x.y.k.E >> ${outdir}/barycenters.dat
    done
done

parent=${parent_prefix}_rotz
if [ -d $parent ]; then
    echo $parent exists - reusing...
else
    mkdir $parent
fi

for ROT in 0 10 30 60 120 300 600 1200
do
    outdir="${parent}/rotz${ROT}arcsec"
    if [ -d ${outdir} ]; then
        echo $outdir exists - reusing...
    else
        mkdir ${outdir}
    fi
    if [ -a ${outdir}/barycenters.dat ]; then
        echo moving old barycenter file out of the way
        mv ${outdir}/barycenters.dat ${outdir}/barycenters.bak
    fi 
    for i in `seq 0 19`
    do
        if [ ! -d ${outdir}/${i} ]; then
                mxrun --mpi=2 -d${outdir}/${i}  ATHENA_1mm.instr dPrz=$ROT dHrz=$ROT SRC=3 porenumber=${porenums[$i]} hyper=1 para=1 positionfile=ATHENA_mm_1line.dat
        fi
        gawk -- '/^[^#]/{sumx+=$3*$1; sumy+=$4*$1; sump+=$1} END {print sumx/sump,sumy/sump, sumx/sump/12*3600/3.1415927, sumy/sump/12*3600/3.1415927}' ${outdir}/${i}/FLmond_list.p.U1.x.y.k.E >> ${outdir}/barycenters.dat
    done
done

