#!bin/bash
version=0.1
containername="astrox:0.1"

DOCK_UID=$(id -u)
DOCK_GID=$(id -g)

docker image build --tag $containername .

XSOCK=/tmp/.X11-unix
#a simple version with out xauth gynmastics
#docker run -u docker -ti -e DISPLAY=$DISPLAY -v $XSOCK:$XSOCK $containername /usr/bin/mxgui

#more sophisticated version which seem to work
XAUTH=/tmp/.docker.xauth
xauth nlist :0 | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -
docker run -u docker -ti -e DISPLAY=$DISPLAY -v $XSOCK:$XSOCK -v $XAUTH:$XAUTH -v $HOME:/home/docker -e XAUTHORITY=$XAUTH --device /dev/dri $containername mxgui
