#!/bin/env bash

#this script is supposed to do a full run of ATHENAs cfgA with _all_
#mirror modules 1 at a time. Required inut files are
# MM_Definitions-1.csv
#    this one comes directly from ESA - and contains the positions of the mirror modules.
# ref_design_breaks
#    this contains definitions of the plates in the mirror modules.
#20 rings => porenumber=1..1062
#15 rings => porenumber=1..678

for e0 in `seq 1 5 6`; do 
   for oa in `seq -w 1 10`; do
       echo mxrun --mpi=4 ATHENA_cfgA_1mm.instr -d ATHENA_cfgA_oa${oa}min_e${e0}keV offaxis_angle=$oa porenumber=1,678 -N678 E0=$e0 reflectivity=\"coating_bilayer_100Ir_80B4C_45sigma.dat\" mmdef_file=\"MM_Definitions-1.csv\"
   done
done
