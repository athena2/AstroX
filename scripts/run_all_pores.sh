#!/bin/sh
mxrun --mpi=32 ATHENA_1pore.instr -d /scratch/erkn/ATHENA_1pore_e1_oa10amin_redo offaxis_angle=-10 SRC=3 PORE=3 E0=1 reflectivity=coating_bilayer_100Ir_80B4C_45sigma.dat porenumber=1,177 -N177
mxrun --mpi=32 ATHENA_1pore.instr -d /scratch/erkn/ATHENA_1pore_e5_oa10amin_redo offaxis_angle=-10 SRC=3 PORE=3 E0=5 reflectivity=coating_bilayer_100Ir_80B4C_45sigma.dat porenumber=1,177 -N177
