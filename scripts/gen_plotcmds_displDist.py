#!/usr/bin/env python
#misalign=[0,10,30,60]
#axis='y'

#misalign=[0,60,120, 300, 600, 1200];axis='y'

misalign=[0,30,60,120,300];axis='x'
nodots=2000

plotcmdtmplt=" \'<head -n%(nodots)d athena_1line_mm_rot%(axis)s/rot%(axis)s%(ma)sarcsec/%(no)d/FLmond_list.p.U1.x.y.k.E\' u ($3/12*3600/3.1415927*180):($4/12*3600/3.1415927*180):(%(no)d+%(angleno)d) w d lc palette notitle, \\\n"

angleno=1

misalign.reverse()
angleno=(len(misalign)-1)*20+1

plotcmd="plot [*:*][*:*] \\\n"
for ma in misalign:
    for no in range(0,20):
        plotcmd=plotcmd +(plotcmdtmplt % locals() )
    angleno-=20

print (plotcmd)

