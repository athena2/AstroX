function [B,lims]=collect_pores(directory,pores, detector_name,weights);
%COLLECT_PORES sum data for a number of PSD_monitor files in a scan
%
%   B=collect_pores('dir', 1:10, 'detector.dat') sums up the scan numbers
%   1 to 10 inclusive found in directory 'dir' using the 'detector.dat' files 

if (nargin>3)
    multiplier=weights.*(ones(size(pores)));
else
    multiplier=ones(size(pores));
end

for i = 1:length(pores)
    pnum=pores(i);
    mm=multiplier(i);

    fname=sprintf('%s/%d/%s',directory,pnum,detector_name);
    if(i==1 | mod(i,50)==0 | i==length(pores))
        fname
    end
    [s,stdo]=system(sprintf('grep type %s',fname));
    dims=sscanf(stdo,'# type: array_%dd');

    b=textread(fname,'','commentstyle','shell');
    Sz=size(b);

    if (pnum == pores(1))
        if (dims==2)
            B=zeros(Sz(2),Sz(2));
        else
            B=zeros(Sz(1),Sz(2));
            B(:,1)=b(:,1);
        end
        [s,stdo]=system(sprintf('grep limits %s',fname));
        lims=sscanf(stdo,'# xylimits: %g %g %g %g');
    end
    if (dims==2)
        B=B+b(1:Sz(2),:)*mm;
    else
        B(:,2:Sz(2))=B(:,2:Sz(2))+b(:,2:Sz(2))*mm;
    end
end


