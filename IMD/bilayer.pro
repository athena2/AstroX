PRO bilayer

print, 'started'
lambda_min = 0.2
lambda_max = 125.
lambda_step = 0.1
lambda = lambda_step*findgen(round((lambda_max-lambda_min)/lambda_step+1))+lambda_min
theta_min = 0.1
theta_max = 10.
theta_step = 0.05
theta = theta_step*findgen(round((theta_max-theta_min)/theta_step+1))+theta_min
optCte = B4C_Ir_SiO(Lambda)
NC = bilayer_OCarray(optCte, Lambda)
SIGMA = 4.5
Z = [100,100]

; ============================================
; IMD
; ============================================
FRESNEL, (90-theta), LAMBDA, NC, Z, SIGMA, INTERFACE, RA=RA
; ============================================

openw, 1, 'coating.dat', width=16*(n_elements(theta)+1)
printf, 1, '#lambda_min='+strtrim(lambda_min,1)
printf, 1, '#lambda_max='+strtrim(lambda_max,1)
printf, 1, '#lambda_step='+strtrim(lambda_step,1)
printf, 1, '#theta_min='+strtrim(theta_min,1)
printf, 1, '#theta_max='+strtrim(theta_max,1)
printf, 1, '#theta_step='+strtrim(theta_step,1)
for i=0, n_elements(lambda)-1 do begin &$
printf, 1, RA(*,i) &$
endfor
close, 1

STOP
END










function B4C_Ir_SiO, arrLambda

nLayer = 2. ; number of layers
oc_arr = dcomplexarr(nLayer+2,n_elements(arrLambda)) ; optical constants array.
for i=0, n_elements(arrLambda)-1 do begin &$
oc_arr(0,i) = complex(1.0,0.) &$; Hardcoded vacuum ambient
endfor
for i=0, n_elements(arrLambda)-1 do begin &$
oc_arr(1,i) = IMD_NK('B4C_llnl_cxro',arrLambda(i)) &$; z1
endfor
for i=0, n_elements(arrLambda)-1 do begin &$
oc_arr(2,i) = IMD_NK('Ir_llnl_cxro',arrLambda(i)) &$; z2
endfor
for i=0, n_elements(arrLambda)-1 do begin &$
oc_arr(3,i) = IMD_NK('SiO_llnl_cxro',arrLambda(i)) &$ ; The substrate
endfor
return, oc_arr
print, 'OK here'

END









function bilayer_OCarray, optCte, Lambda

optc_array = dcomplexarr(4, n_elements(Lambda)) ; optical constants array.
optc_array(0,*) = optCte(0,*) ; Hardcoded vacuum ambient
optc_array(1,*) = optCte(1,*) ; z1
optc_array(2,*) = optCte(2,*) ; z2
optc_array(3,*) = optCte(3,*) ; The substrate
return, optc_array

END
