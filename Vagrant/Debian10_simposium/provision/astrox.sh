#!/bin/bash

sudo apt-get install -y rsync zip

#checkout the astrox-tree
cd /home/vagrant
sudo -u vagrant  git clone https://gitlab.com/ATHENA2/astrox.git --depth=1
cd astrox
sudo -u vagrant ./mkdist.sh x.y
ls mcxtrace-astrox-comps/staging

cd /usr/share/mcxtrace/1.5/
sudo tar -xvzf ~vagrant/astrox/mcxtrace-astrox-comps/staging/astrox_mcxtrace_x.y-1.5.tar.gz
