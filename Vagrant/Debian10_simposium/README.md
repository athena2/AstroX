#Debian 10 based vagrant description
To build and provision this machine you first need to get a copy of the SPORT
repository from gitlab.
This may be done using the following command:
```git clone --depth=1 git@gitlab.com/ATHENA2/SPORT .```


Next thing is to run the vagrant command building the box:
```vagrant up --provision```
This starts from a clean Debian 10 box and provisions it by installing McXtrace, its AstroX add-on, and SPORT.

The intention is to push this as a public Vagrant box to be downloaded automatically. 
