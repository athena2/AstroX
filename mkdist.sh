#!/usr/bin/env bash

asx_version=$1
mcx_version=1.5

if [ "x$1" == "x" ];
then
   echo Usage:
   echo "    mkdist.sh VERSION"
   exit 0
fi


cd mcxtrace-astrox-comps

#for dir in `ls -d *`;
#do
#     ${dir}files=`ls $dir`
#done

opticsfiles=`ls optics/Pore_[chp].comp optics/MM_[chp].comp optics/Shell_[chp].comp optics/Ring_[chp].comp`
examplesfiles=`ls examples/*.instr`
datafiles=`ls data/*`
sourcesfiles=`ls sources/*.comp`

tgt_dirs="optics sources data examples"

if [ -e "staging" ];  
then
        echo staging directory exists - moving it out of the way to staging.bak
        rm -rf staging.bak
        mv staging staging.bak
fi 
mkdir staging

for dire in $tgt_dirs; do
        dire2=${dire}files
        rsync -a --info=progress2 ${dire} staging/
        cd staging
        zip astrox_mcxtrace_${asx_version}-${mcx_version}.zip ${dire}/*
        tar -rf astrox_mcxtrace_${asx_version}-${mcx_version}.tar ${dire}/*
        cd ..
done
gzip staging/astrox_mcxtrace_${asx_version}-${mcx_version}.tar

cd ..

